#version 330

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat3 normalMatrix;

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec3 vertexNormal;

out vec4 color;

void main()
{
    vec3 normalWorldSpace = normalize(normalMatrix * vertexNormal);
    // цвет как просилось в описании задания
    color.rgb = (normalWorldSpace.xyz + 0.5) * 0.5;
    // непрозрачные стены
    color.a = 1.0;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(vertexPosition, 1.0);
}
