#include "MazeCameraMover.hpp"

#include <algorithm>

MazeCameraMover::MazeCameraMover(Maze &maze, bool free) :
        CameraMover(),
        maze(maze),
        _pos(maze.startXPosition, maze.startYPosition, (free ? 7.0f : 0.5f)),
        _free(free)
{
    _rot = glm::toQuat(glm::lookAt(_pos, glm::vec3(0.0f, 0.0f, 0.5f), glm::vec3(0.0f, 0.0f, 1.0f)));
}

void MazeCameraMover::handleKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
}

void MazeCameraMover::handleMouseMove(GLFWwindow* window, double xpos, double ypos)
{
    int state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    if (state == GLFW_PRESS)
    {
        double dx = xpos - PreviousXPosition;
        double dy = ypos - PreviousYPosition;

        //Добавляем небольшой поворот вверх/вниз
        glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;
        _rot *= glm::angleAxis(static_cast<float>(dy * 0.002), rightDir);

        //Добавляем небольшой поворов вокруг вертикальной оси
        glm::vec3 upDir(0.0f, 0.0f, 1.0f);
        _rot *= glm::angleAxis(static_cast<float>(dx * 0.002), upDir);
    }

    PreviousXPosition = xpos;
    PreviousYPosition = ypos;
}

void MazeCameraMover::handleScroll(GLFWwindow* window, double xoffset, double yoffset)
{
}

void MazeCameraMover::update(GLFWwindow* window, double dt)
{
    float speed = 1.2f;

    //Получаем текущее направление "вперед" в мировой системе координат
    glm::vec3 forwDir = glm::vec3(0.0f, 0.0f, -1.0f) * _rot;

    //Получаем текущее направление "вправо" в мировой системе координат
    glm::vec3 rightDir = glm::vec3(1.0f, 0.0f, 0.0f) * _rot;

    glm::vec3 delta;

    //Двигаем камеру вперед/назад
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    {
        delta += forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    {
        delta -= forwDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    {
        delta -= rightDir * speed * static_cast<float>(dt);
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    {
        delta += rightDir * speed * static_cast<float>(dt);
    }
//    std::cerr << _pos[0] << ' ' << _pos[1] << std::endl;
    if (!_free)
    {
        if (delta[0] > 0 &&
            (int(_pos[0] + std::max(0.15f, delta[0])) > maze.GetHeight()-1 ||
             maze[_pos[0] + std::max(0.15f, delta[0])][_pos[1]]) ||
            delta[0] < 0 && ((_pos[0] + std::min(-0.15f, delta[0])) < 0 ||
                             maze[_pos[0] + std::min(-0.15f, delta[0])][_pos[1]]))
        {
            delta[0] = 0;
        }
        if (delta[1] > 0 &&
            (int(_pos[1] + std::max(0.15f, delta[1])) > maze.GetWidth()-1 ||
             maze[_pos[0]][_pos[1] + std::max(0.15f, delta[1])]) ||
            delta[1] < 0 && ((_pos[1] + std::min(-0.15f, delta[1])) < 0 ||
                             maze[_pos[0]][_pos[1] + std::min(-0.15f, delta[1])]))
        {
            delta[1] = 0;
        }
        delta[2] = 0;
    }

    _pos += delta;

    //-----------------------------------------

    //Соединяем перемещение и поворот вместе
    _camera.viewMatrix = glm::toMat4(-_rot) * glm::translate(-_pos);

    //-----------------------------------------

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    //Обновляем матрицу проекции на случай, если размеры окна изменились
    _camera.projMatrix = glm::perspective(glm::radians(45.0f), (float)width / height, 0.1f, 100.f);
}

void MazeCameraMover::move(glm::vec3 pos)
{
    _pos = pos;
}
