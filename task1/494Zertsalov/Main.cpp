#include "MazeCameraMover.hpp"

#include "common/Application.hpp"
#include "common/ShaderProgram.hpp"

#include <glm/ext.hpp>

#include <iostream>
#include <fstream>
#include <vector>

/**
Проект №3: Лабиринт
*/

class Faces {
    std::vector<glm::mat4> _modelMatrices;
    GLuint _vao;

public:
    // описание одного квадрата из которых мы будем собирать лабиринт
    float _points[36] = {
            // координаты углов первого треугольника
            -0.5f, -0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            0.5f, -0.5f, 0.0f,
            // координаты углов второго треугольника
            -0.5f, -0.5f, 0.0f,
            -0.5f, 0.5f, 0.0f,
            0.5f, 0.5f, 0.0f,
            // нормали направленные вверх
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
            0.0f, 0.0f, 1.0f,
    };

    Faces() {}

    void load() {
        GLuint vbo;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glBufferData(GL_ARRAY_BUFFER, 36 * sizeof(float), _points, GL_STATIC_DRAW);

        glGenVertexArrays(1, &_vao);
        glBindVertexArray(_vao);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);

        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void *>(0));
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, reinterpret_cast<void *>(18 * sizeof(float)));

        glBindVertexArray(0);
    }

    void add(glm::mat4 mat) {
        _modelMatrices.push_back(mat);
    }

    void draw(ShaderProgramPtr shader) {
        glBindVertexArray(_vao);
        for (auto modelMatrix : _modelMatrices) {
            shader->setMat4Uniform("modelMatrix", modelMatrix);
            shader->setMat3Uniform("normalMatrix", glm::transpose(glm::inverse(modelMatrix)));

            glDrawArrays(GL_TRIANGLES, 0, 6);
        }
    }
};

class MazeApplication : public Application {
protected:
    Faces _faces;

    ShaderProgramPtr _shader;
    std::shared_ptr<CameraMover> _camera_active;
    std::shared_ptr<CameraMover> _camera_inactive;

public:
    MazeApplication(Maze &maze)
            : Application(std::make_shared<MazeCameraMover>(maze)), _camera_active(_cameraMover),
              _camera_inactive(std::make_shared<MazeCameraMover>(maze, true)) {
        create_faces(maze);
    }

    ~MazeApplication() {}

    void create_faces(Maze &maze) {
        const unsigned long height = maze.GetHeight();
        const unsigned long width = maze.GetWidth();

        glm::mat4 floor = glm::translate(glm::mat4(), glm::vec3(0.5f, 0.5f, 0.0f));
        glm::mat4 ceil = glm::translate(glm::mat4(), glm::vec3(0.5f, 0.5f, 1.0f)) *
                         // поворот на pi вокруг х оси
                         glm::rotate(glm::mat4(), glm::pi<float>(), glm::vec3(1.0f, 0.0f, 0.0f));
        glm::mat4 south = glm::translate(glm::mat4(), glm::vec3(0.0f, 0.5f, 0.5f)) *
                          // на -pi / 2 вокруг y оси
                          glm::rotate(glm::mat4(), -glm::pi<float>() / 2.0f, glm::vec3(0.0f, 1.0f, 0.0f));
        glm::mat4 north = glm::translate(glm::mat4(), glm::vec3(1.0f, 0.5f, 0.5f)) *
                          // на pi / 2 вокруг y оси
                          glm::rotate(glm::mat4(), glm::pi<float>() / 2.0f, glm::vec3(0.0f, 1.0f, 0.0f));
        glm::mat4 east = glm::translate(glm::mat4(), glm::vec3(0.5f, 0.0f, 0.5f)) *
                         // на pi / 2 вокруг х оси
                         glm::rotate(glm::mat4(), glm::pi<float>() / 2.0f, glm::vec3(1.0f, 0.0f, 0.0f));
        glm::mat4 west = glm::translate(glm::mat4(), glm::vec3(0.5f, 1.0f, 0.5f)) *
                         // на -pi / 2 ворку х оси
                         glm::rotate(glm::mat4(), -glm::pi<float>() / 2.0f, glm::vec3(1.0f, 0.0f, 0.0f));

        for (int i = 0; i < height; ++i) {
            for (int j = 0; j < width; ++j) {
                glm::mat4 move = glm::translate(glm::mat4(), glm::vec3(i, j, 0));
                // для всех не свободных клеток нужен верх и низ
                if (!maze[i][j]) {
                    _faces.add(move * floor);
                    _faces.add(move * ceil);
                }
                if ((i > 0 && maze[i][j] && !maze[i - 1][j]) ||
                    (i == 0 && !maze[i][j])) {
                    _faces.add(move * south);
                }
                if ((i < height - 1 && maze[i][j] && !maze[i + 1][j]) ||
                    (i == height - 1 && !maze[i][j])) {
                    _faces.add(move * north);
                }
                if ((j > 0 && maze[i][j] && !maze[i][j - 1]) ||
                    (j == 0 && !maze[i][j])) {
                    _faces.add(move * east);
                }
                if ((j < width - 1 && maze[i][j] && !maze[i][j + 1] ||
                     j == width - 1 && !maze[i][j])) {
                    _faces.add(move * west);
                }
            }
        }
    }

    void makeScene() override {
        Application::makeScene();

        _faces.load();

        _shader = std::make_shared<ShaderProgram>(
                "494ZertsalovData/shaderNormal.vert",
                "494ZertsalovData/shader.frag");
    }

    void draw() override {
        Application::draw();

        //Получаем размеры экрана (окна)
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        //Устанавливаем порт вывода на весь экран (окно)
        glViewport(0, 0, width, height);

        //Очищаем порт вывода (буфер цвета и буфер глубины)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        //Подключаем VertexArrayObject с настойками полигональной модели

        //Подключаем шейдер
        _shader->use();

        _shader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _shader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _faces.draw(_shader);

    }


    void handleKey(int key, int scancode, int action, int mods) {
        if (action == GLFW_PRESS) {
            if (key == GLFW_KEY_C) {
                _camera_active.swap(_camera_inactive);
                _cameraMover = _camera_active;
            }
        }
        Application::handleKey(key, scancode, action, mods);
    }
};

int main() {
    Maze maze("494ZertsalovData/maze.txt");
    MazeApplication app(maze);
    app.start();

    return 0;
}
